import java.util.ArrayList;

public class OSPFPacket {
    int version;
    int type;
    int length;
    long routerID;
    long areaID;
    int checksum;
    LSAPacket[] lsaPackets;
    final static int TYPE_HELLO = 1;
    final static int TYPE_DB_DESC = 2;
    final static int TYPE_REQ = 3;
    final static int TYPE_UPDATE = 4;
    final static int TYPE_ACK = 5;

    public OSPFPacket(byte[] data, int offset){
        this.version = NetUtils.getByte(data, offset);
        this.type = NetUtils.getByte(data, offset + 1);
        this.length = NetUtils.getShort(data, offset + 2);
        this.routerID = NetUtils.getInt(data, offset + 4);
        this.areaID = NetUtils.getInt(data, offset + 8);
        this.checksum = NetUtils.getShort(data, offset + 12);
        if (!verifyChecksum(data, offset, this.length))
            return;

        //loading LSA packets
        if (type == TYPE_UPDATE){
            long l = NetUtils.getInt(data, offset + 24);
            System.out.println("-------------------------------------" + l +  "------------------------------------------");
            ArrayList<LSAPacket> lsaPacketsList = new ArrayList<>();
            LSAPacket lsaPacket;
            int i = offset + 28;
            for (int j = 0; j < l; j++){
                lsaPacket = new LSAPacket();
                i = lsaPacket.load(data, i);
                if (lsaPacket.getType() == LSAPacket.TYPE_ROUTER){
                    lsaPacketsList.add(lsaPacket);
                }
            }
            lsaPackets = lsaPacketsList.toArray(new LSAPacket[lsaPacketsList.size()]);
        }
        else{
            lsaPackets = null;
        }
    }
    public boolean verifyChecksum(byte[] data, int offset, int length){
        return true;
    }
}
