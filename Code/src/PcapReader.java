import org.jnetpcap.Pcap;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.util.PcapPacketArrayList;

public class PcapReader {
    String file_name;
    public PcapReader(String file_name){
        this.file_name = file_name;
    }
    public PcapPacketArrayList readFile() throws Exception{
        PcapPacketArrayList packets = new PcapPacketArrayList();
        final StringBuilder errorBuffer = new StringBuilder();
        Pcap pcap = Pcap.openOffline(file_name, errorBuffer);

        if (pcap == null)
            throw new Exception("Error at reading file!");
        PcapPacketHandler<PcapPacketArrayList> packetHandler = new PcapPacketHandler<PcapPacketArrayList>() {
            public void nextPacket(PcapPacket packet, PcapPacketArrayList packets){
//                System.out.println("hoy!" + packet.toString());
                packets.add(packet);
            }
        };
        pcap.loop(-1, packetHandler, packets);
        pcap.close();
        return packets;
    }
}
