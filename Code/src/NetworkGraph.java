import java.util.*;

public class NetworkGraph {
    public HashMap<Long, HashSet<Long>> linksMembers;
    public TreeMap<Long, TreeSet<Long>> adjacencyMatrix;
    public NetworkGraph(){
        linksMembers = new HashMap<>();
        adjacencyMatrix = new TreeMap<>();
    }
    public void add(Long link, Long member){
        if (linksMembers.containsKey(link)){
            linksMembers.get(link).add(member);
        } else {
            HashSet<Long> nhash = new HashSet<>();
            nhash.add(member);
            linksMembers.put(link, nhash);
        }
    }
    public void create(){
        for (Map.Entry<Long, HashSet<Long>> e : linksMembers.entrySet()) {
            Iterator it = e.getValue().iterator();
            ArrayList<Long> members = new ArrayList<>();
            while (it.hasNext()) {
                members.add((Long)it.next());
            }
            for (int i = 0; i < members.size(); i++) {
                if (!adjacencyMatrix.containsKey(members.get(i))) {
                    adjacencyMatrix.put(members.get(i), new TreeSet<Long>());
                }
            }
            for (int i = 0; i < members.size(); i++){
                for (int j = 0; j < members.size(); j++){
                    adjacencyMatrix.get(members.get(i)).add(members.get(j));
                }
            }
        }
    }
    public int[][] fullAdjacencyMatrix(){
        int[][] fullMat = new int[adjacencyMatrix.size()][];
        for (int i = 0; i < fullMat.length; i++){
            fullMat[i] = new int[adjacencyMatrix.size()];
            Arrays.fill(fullMat[i], 0);
        }
        HashMap<Long, Integer> orderMap = new HashMap<>();
        int i = 0;
        for (Map.Entry<Long, TreeSet<Long>> e : adjacencyMatrix.entrySet()){
            orderMap.put(e.getKey(), i);
            i++;
        }
        for (Map.Entry<Long, TreeSet<Long>> e : adjacencyMatrix.entrySet()){
            Iterator<Long> it = e.getValue().iterator();
            while (it.hasNext()){
                fullMat[orderMap.get(e.getKey())][orderMap.get(it.next())] = 1;
            }
        }
        for (int j = 0; j < fullMat.length; j++){
            fullMat[j][j] = 0;
        }
        return fullMat;
    }
    public String toRawString(){
        StringBuilder str = new StringBuilder("");
        for (Map.Entry<Long, HashSet<Long>> e : linksMembers.entrySet()){
            str.append(NetUtils.IPFormat(e.getKey()));
            str.append(" : ");
            Iterator it = e.getValue().iterator();
            while (it.hasNext()){
                str.append(NetUtils.IPFormat((Long)it.next()));
                str.append(" , ");
            }
            str.append("\n");
        }
        return str.toString();
    }
    public String toString(){
        StringBuilder str = new StringBuilder("");
        for (Map.Entry<Long, TreeSet<Long>> e : adjacencyMatrix.entrySet()){
            str.append(NetUtils.IPFormat(e.getKey()));
            str.append(" : ");
            Iterator it = e.getValue().iterator();
            while (it.hasNext()){
                str.append(NetUtils.IPFormat((Long)it.next()));
                str.append("  ,  ");
            }
            str.append("\n");
        }
        return str.toString();
    }
}
