import javafx.util.Pair;

import java.util.ArrayList;

public class NetLink {
    final static int TYPE_P2P = 1;
    final static int TYPE_TRANSIT = 2;
    final static int TYPE_STUB = 3;
    final static int TYPE_VLINK = 4;
    int type;
    long ID;
    long data;
    ArrayList<Pair<Integer, Integer>> TOSs = new ArrayList<>();
    int TOS0Metric;
    public int getType(){
        return this.type;
    }
    public int load(byte[] data, int offset){

        this.ID = NetUtils.getInt(data, offset);
        this.data = NetUtils.getInt(data, offset + 4);
        this.type = NetUtils.getByte(data, offset + 8);
        int numTOS = NetUtils.getByte(data, offset + 9);
        this.TOS0Metric = NetUtils.getShort(data,offset + 10);
        for (int i = 0; i < numTOS; i++){
            TOSs.add(new Pair<Integer, Integer>(NetUtils.getByte(data, offset + 12 + i * 4), NetUtils.getShort(data, offset + 14 + i * 4)));
        }
        return offset + 12 + numTOS * 4;
    }
}
