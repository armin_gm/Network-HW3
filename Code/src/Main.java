import org.jnetpcap.Pcap;
import org.jnetpcap.nio.JMemory;
import org.jnetpcap.packet.*;
import org.jnetpcap.protocol.tcpip.Http;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.util.PcapPacketArrayList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
//    final String file_name = "Packets.pcap";
//    final StringBuilder errorBuffer = new StringBuilder();
//    final Pcap pcap = Pcap.openOffline(file_name, errorBuffer);
//        if (pcap == null){
//        System.out.println(errorBuffer.toString());
//        return;
//    }
//
//        pcap.loop(10, new JPacketHandler<StringBuilder>() {
//        final Tcp tcp = new Tcp();
//        final Http http = new Http();
//    public void nextPacket(JPacket packet, StringBuilder errorBuffer){
//        if (packet.hasHeader(tcp)){
//            System.out.println("tcp header: " + tcp.toString());
//        } else {
//            System.out.println("no tcp header!");
//        }
//        System.out.println("frame " + packet.getFrameNumber());
//    }
//}, errorBuffer);
//        JScanner.getThreadLocal().setFrameNumber(0);
//final PcapPacket packet = new PcapPacket(JMemory.POINTER);
//final Tcp tcp = new Tcp();
//        for (int i = 0; i < 5; i++){
//        pcap.nextEx(packet);
//        if (packet.hasHeader(tcp)){
//        System.out.printf("#%d seq=%08X%n", packet.getFrameNumber(), tcp.seq());
//        } else {
//        System.out.println("no tcp header! -- ");
//        }
//        }
    public static void main(String[] args) throws Exception {
        PcapReader pcapReader = new PcapReader("Data/Packets.pcap");
        PcapPacketArrayList packets = pcapReader.readFile();
        OSPFPacket ospfPacket;
        Payload payload = new Payload();
        NetLink[] lll;
        NetworkGraph networkGraph = new NetworkGraph();
        for (PcapPacket packet : packets){
            if (packet.hasHeader(payload)){
                printBytes(payload.getByteArray(0, payload.getLength()));
                ospfPacket = new OSPFPacket(payload.getByteArray(0, payload.getLength()), 0);
                if (ospfPacket.lsaPackets != null) {
                    for (LSAPacket lsaPacket : ospfPacket.lsaPackets){
                        for (NetLink netLink : lsaPacket.links){
                            networkGraph.add(netLink.ID, lsaPacket.lsID);
                            System.out.print("");
                        }
                    }
                }
            }
        }
        System.out.println(networkGraph.toRawString());
        networkGraph.create();
        System.out.println(networkGraph.toString());
        int[][] mat = networkGraph.fullAdjacencyMatrix();
        for (int[] item : mat){
            for (int subItem : item){
                System.out.print(subItem + " ");
            }
            System.out.println();
        }
        writeAdjacencyMatix("test.txt", mat);
    }
    public static void printBytes(byte[] data){
        for (int i = 0; i < data.length; i++){
            System.out.print(String.format("%02X", data[i]) + " ");
            if ((i + 1) % 4 == 0)
                System.out.println();
        }
    }
    public static void writeAdjacencyMatix(String file_name, int[][] matrix) throws IOException{
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file_name)));
        for (int j = 0; j < matrix.length; j++){
            for (int i = 0; i < matrix[j].length; i++){
                System.out.print(String.valueOf(matrix[j][i]));
                bw.write(String.valueOf(matrix[j][i]));
                if (i != matrix[j].length - 1){
                    bw.write(",");
                    System.out.print(",");
                } else {
                    if (j != matrix.length - 1){
                        bw.write("\n");
                        System.out.print("\n");
                    }
                }
            }
        }
        bw.flush();
        bw.close();
    }
}
