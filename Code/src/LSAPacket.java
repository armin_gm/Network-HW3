import java.util.ArrayList;

public class LSAPacket {
    final static int TYPE_ROUTER = 1;
    final static int TYPE_NETWORK = 2;
    final static int TYPE_SUMMARY_IP = 3;
    final static int TYPE_SUMMARY_ASBR = 4;
    final static int TYPE_EXTERNAL = 5;
    int age;
    int options;
    int type;
    long lsID;
    long advertisingRouter;
    long seqNumber;
    int checksum;
    int length;
    boolean[] flags = new boolean[3]; // V-E-B
    NetLink[] links = null;
    public int getType(){
        return this.type;
    }
    public LSAPacket(){ }

    /**
     * loads LSA from byte array
     * @param data byte array being loaded
     * @param offset the offset from where data is loaded
     * @return the index of the byte after last byte of the data loaded from byte array, used to load next LSAs
     */
    public int load(byte[] data, int offset){
        age = NetUtils.getShort(data, offset);
        options = NetUtils.getByte(data, offset + 2);
        type = NetUtils.getByte(data, offset + 3);
        lsID = NetUtils.getInt(data, offset + 4);
        advertisingRouter = NetUtils.getInt(data, offset + 8);
        seqNumber = NetUtils.getInt(data, offset + 12);
        checksum = NetUtils.getShort(data, offset + 16);
        length = NetUtils.getShort(data, offset + 18);
        int tmp;
        int out = offset + length;
        if (this.type == TYPE_ROUTER){
            tmp = NetUtils.getShort(data, offset + 20);
            flags[0] = NetUtils.toBoolean(tmp, 10);
            flags[1] = NetUtils.toBoolean(tmp, 9);
            flags[2] = NetUtils.toBoolean(tmp, 8);

            //loading stub & transmit links
            int l = NetUtils.getShort(data, offset + 22);
            System.out.println("###############################################" + l +  "################################################3");
            NetLink link;
            ArrayList<NetLink> linkArrayList = new ArrayList<>();
            int i = offset + 24;
            for (int j = 0; j < l; j++){
                link = new NetLink();
                i = link.load(data, i);
                if (link.getType() == NetLink.TYPE_STUB || link.getType() == NetLink.TYPE_TRANSIT){
                    linkArrayList.add(link);
                }
            }
            out = i;
            links = linkArrayList.toArray(new NetLink[linkArrayList.size()]);
        }
        return out;
    }
}
