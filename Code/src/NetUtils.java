import org.jnetpcap.packet.Payload;

public class NetUtils {
    public static int getShort(byte[] data, int offset){
        System.out.println(((data[offset] & 0x000000FF) << 8) | (data[offset + 1] & 0x000000FF));
        return ((data[offset] & 0x000000FF) << 8) | (data[offset + 1] & 0x000000FF);
    }
    public static long getInt(byte[] data, int offset){
        return ((data[offset] << 24) | ((data[offset + 1] << 24) >>> 8)
                | ((data[offset + 2] << 24) >>> 16) | (data[offset + 3] & 0x000000FF)) & 0x00000000FFFFFFFFL;
    }
    public static int getByte(byte[] data, int offset){
        return 0x000000FF & data[offset];
    }
    public static boolean toBoolean(int data, int offsetFormRight){
        int x = (data << (31 - offsetFormRight)) >>> 31;
        if (x == 1){
            return true;
        } else {
            return false;
        }
    }
    public static String IPFormat(long IP){
        long x4 = IP % 256;
        IP /= 256;
        long x3 = IP % 256;
        IP /= 256;
        long x2 = IP % 256;
        IP /= 256;
        long x1 = IP % 256;
        IP /= 256;
        return String.valueOf(x1) + "." + String.valueOf(x2) + "." + String.valueOf(x3) + "." + String.valueOf(x4);
    }
}
